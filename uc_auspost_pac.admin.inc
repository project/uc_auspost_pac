<?php
/**
 * @file
 * Administration of the Aus Post Postage Assessment Calculator.
 */

/**
 * Configure module settings including API key. Most options not available
 * until API Key is validated.
 */
function uc_auspost_pac_config_form($form, &$form_state) {

  // Aus Post PAC/PCS API Key.
  $api_key = variable_get('uc_auspost_pac_var_api_key', '');
  $form['keys'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Key'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,

    'api_key' => array(
      '#type' => 'textfield',
      '#title' => t('Postage Assessment Calculation/PCS API Key'),
      '#description' => t('API Key can be obtained at the <a href="@url">Australia Post Developer Centre</a>. API Keys consist of letters, numbers, and sometimes dashes. Your key will be validated with AusPost.', array(
        '@url' => 'https://auspost.com.au/devcentre/pacpcs-registration.asp',
      )),
      '#required' => TRUE,
      '#default_value' => $api_key,
    ),

    // Any change in the key must be validated.
    'api_key_old' => array(
      '#type' => 'value',
      '#value' => $api_key,
    ),
  );

  if ($api_key) {
    $form['keys']['#description'] = t('Changing the API Key will result in reverification of the key.');

    // Caching settings.
    $use_cache_getset = variable_get('uc_auspost_pac_var_use_cache_getset', FALSE);
    $form['caching'] = array(
      '#type' => 'fieldset',
      '#title' => t('Glacial Cache: Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,

      'use_cache_getset' => array(
        '#type' => 'checkbox',
        '#title' => t('Use system cache_set/cache_get functions for Glacial Cache.'),
        '#description' => t('This settings is only useful if your site uses memcache or other non-default caching strategies. If you are only using the Drupal database caching, this should be left off. See README or <a href="@url">#2416249</a> for more information.', array('@url' => url('https://www.drupal.org/node/2416249'))),
        '#default_value' => $use_cache_getset ? 1 : 0,
      ),
    );

    $country_codes = uc_auspost_pac_glacial_get('appac:country');
    $country_codes = ($country_codes ? $country_codes->data : array());
    $source_countries = variable_get('uc_auspost_pac_var_source_countries', array('US', 'GB'));
    $default_country_codes = array();
    foreach ($source_countries as $country) {
      $default_country_codes[$country] = $country;
    }

    // Inform the user about the current state of the glacial cache.
    $glacial_info = _uc_auspost_pac_glacial_cache_info();

    $form['glacial'] = array(
      '#type' => 'fieldset',
      '#title' => t('Glacial Cache: Rebuild'),
      '#description' => t('A cache of mostly static data sourced from the AusPost API is kept permanently on the system to provide a list of available shipping methods, and avoid unnecessary API calls. This data very rarely changes, with any announced changes included in module updates. You can manually rebuild this cache with the given settings using the buttoms below. <strong>You will most likely never need to do this.</strong> These buttons do not save any settings, such as a new API Key entered above. Reverting to the default cache re-installs the static elements hard coded into the module.<br />If you use memcache on your site, please see the README on enabling use of that system with this data.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,

      'info' => array(
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => t('The static elements in your Glacial Cache are: <strong>!gcs_name</strong> - !gcs_desc', array(
          '!gcs_name' => $glacial_info['name'],
          '!gcs_desc' => $glacial_info['description']
        )),
        '#weight' => 0,
      ),

      'glacial_actions' => array(
        '#type' => 'actions',
        '#weight' => 10,

        'rebuild' => array(
          '#type' => 'submit',
          '#value' => t('Rebuild Glacial Cache'),
          // Re-use the existing validation of the API Key to make sure that
          // flushing will work.
          '#validate' => array('uc_auspost_pac_config_form_validate'),
          '#submit' => array('uc_auspost_pac_rebuild_glacial_cache'),
        ),

        'revert' => array(
          '#type' => 'submit',
          '#value' => t('Revert to default cache'),
          // Do not need to validate this submission.
          '#submit' => array('uc_auspost_pac_revert_glacial_cache'),
        ),
      ),

      'advanced' => array(
        '#type' => 'fieldset',
        '#title' => t('Advanced regeneration settings'),
        '#description' => t('These settings are only used when re-generating the the long term cache and are not used during normal function. Do not change these settings unless you have reviewed the code and understand the concequences.'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#weight' => 15,

        'from_postcode' => array(
          '#type' => 'textfield',
          '#title' => t('From postcode'),
          '#description' => t('The from_postcode argument when requesting the Domestic Parcel Service list.'),
          '#element_validate' => array('element_validate_integer_positive'),
          '#required' => TRUE,
          '#default_value' => variable_get('uc_auspost_pac_var_from_postcode', 2000),
        ),

        'to_postcode' => array(
          '#type' => 'textfield',
          '#title' => t('To postcode'),
          '#description' => t('The to_postcode argument when requesting the Domestic Parcel Service list.'),
          '#element_validate' => array('element_validate_integer_positive'),
          '#required' => TRUE,
          '#default_value' => variable_get('uc_auspost_pac_var_to_postcode', 3000),
        ),

        'source_countries' => array(
          '#type' => 'select',
          '#title' => t('Country codes'),
          '#description' => t('The country_code values for use when requesting the Internation Letter and Parcel Service lists. Ctrl-click to multi-select. Keep the number selected low (default is 2 - US,GB).'),
          '#options' => $country_codes,
          '#multiple' => TRUE,
          '#required' => !empty($country_codes),
          '#default_value' => $default_country_codes,
        ),
      ),
    );
  }

  $form['actions'] = array(
    '#type' => 'actions',

    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save settings'),
    ),
  );

  return $form;
}

/**
 * Validation handler for module settings form.
 *
 * @see uc_auspost_pac_config_form()
 */
function uc_auspost_pac_config_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  // If the API Key has changed, refresh the package information to verify that
  // it is still valid.
  if ($values['api_key'] != $values['api_key_old']) {

    // API keys have changed to be all lower case letters and numbers, with
    // dashes separating parts. The length has increased to 36 chars too.
    // Formerly 32 chars long, upper and lower case letters and numbers.
    if (!preg_match('/^[0-9A-Za-z-]*$/', $values['api_key'])) {
      form_set_error('api_key', t('API Key should consist of letters, numbers and dashes only.'));
      // No more checking needed.
      return;
    }

    // Verify that the API Key is valid by trying a known valid quote request.
    // Example provided from the Tech Spec v1.3.
    // letter/domestic/service.json?length=114&width=162&thickness=4&weight=20.
    module_load_include('inc', 'uc_auspost_pac', 'uc_auspost_pac.quote');
    $letter_test = _uc_auspost_pac_api('letter/domestic/service', array(
      'length' => 114,
      'width' => 162,
      'thickness' => 4,
      'weight' => 20,
    ), TRUE, UC_AUSPOST_PAC_CACHE_NONE, $values['api_key']);
    if ($letter_test['error']) {
      form_set_error('api_key', t(
        'API server returned error when testing your key. AusPost PAC said "@error"', array(
          '@error' => $letter_test['error'],
        )
      ));
    }
  }

  // @todo Use the PCS API to verify the postcodes are correct. For now, they
  // are only validated as positive integers.
  /*
  if (isset($values['from_postcode'])) {
    // The from_postcode and to_postcode must be valid.
  }
  */

  // Process the countries list into the expect form.
  if (isset($values['source_countries'])) {
    $form_state['values']['source_countries'] = array_keys(array_filter($values['source_countries']));
  }
}

/**
 * Submit handler for module settings form.
 *
 * @see uc_auspost_pac_config_form()
 */
function uc_auspost_pac_config_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  $keys = array(
    'api_key',
    'use_cache_getset',
    'from_postcode',
    'to_postcode',
    'source_countries',
  );
  foreach ($keys as $key) {
    // Certain keys may not exist based on the state of the form.
    if (isset($values[$key])) {
      variable_set('uc_auspost_pac_var_' . $key, $values[$key]);
    }
  }

  drupal_set_message(t('Settings have been saved.'));

  // If the glacial cache does not contain the marker data that says things
  // should be built, then rebuild all the data as a batch process.
  if (!($cache = uc_auspost_pac_glacial_get('appac:built-marker'))) {
    drupal_set_message(t('Rebuilding missing data from API server.'));

    module_load_include('inc', 'uc_auspost_pac', 'uc_auspost_pac.batch');
    batch_set(_uc_auspost_pac_batch_init());
  }
}

/**
 * Submit handler to flush caches and then rebuild the Glacial Cache values
 * from the API server.
 */
function uc_auspost_pac_rebuild_glacial_cache($form, &$form_state) {

  // Flush the caches regardless because they potentially contain values that
  // will no longer be valid after this rebuild.
  drupal_flush_all_caches();

  module_load_include('inc', 'uc_auspost_pac', 'uc_auspost_pac.batch');
  batch_set(_uc_auspost_pac_batch_init());
}

/**
 * Submit handler to revert the glacial cache back to the one provided in the
 * install or update process.
 */
function uc_auspost_pac_revert_glacial_cache($form, &$form_state) {

  module_load_include('inc', 'uc_auspost_pac', 'uc_auspost_pac.static_data');
  _uc_auspost_pac_install_prebuilt_cache();

  drupal_set_message(t('Glacial cache elements reverted to defaults.'));
}

/**
 * Configuration form to change settings specific to the shipping method.
 */
function uc_auspost_pac_config_method_form($form, &$form_state, $method_id) {

  // Get the settings for this method. $method_id variable can be trusted
  // because this form is normally only called from statically entered menu
  // items.
  $settings = _uc_auspost_pac_get_method_settings($method_id);

  $form = array();

  $form['method_id'] = array(
    '#type' => 'value',
    '#value' => $method_id,
  );
  $form['settings'] = array(
    '#type' => 'value',
    '#value' => $settings, 
  );

  // Packing method; pulled from the packing method info.
  $packing_methods = uc_auspost_pac_get_packing_method();
  $options = array();
  foreach ($packing_methods as $id => $method) {
    $ops = '';
    if (isset($method['config'])) {
      $ops = l(t('settings'), 'admin/store/settings/quotes/auspost-pac/packing/' . $id . '/' . $method_id, array('query' => drupal_get_destination()));
    }
    $options[$id] = array(
      'name' => $method['title'],
      'desc' => isset($method['description']) ? $method['description'] : '',
      'ops' => $ops,
    );
  }

  $form['packing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Packing Method'),

    'packing_method' => array(
      '#type' => 'tableselect',
      '#header' => array(
        'name' => t('Name'),
        'desc' => t('Description'),
        'ops' => t('Ops'),
      ),
      '#options' => $options,
      '#multiple' => FALSE,
      '#required' => TRUE,
      '#default_value' => $settings['packing_method'],
    ),
  );

  $form['markup'] = array(
    '#type' => 'fieldset',
    '#title' => t('Post-quote adjustments'),
    '#description' => t('A fixed or percentage amount can be added to the returned shipping rate.'),

    'markup_type' => array(
      '#type' => 'select',
      '#title' => t('Markup type'),
      '#options' => array(
        UC_AUSPOST_PAC_MARKUP_NONE => t('None'),
        UC_AUSPOST_PAC_MARKUP_PERCENT => t('Percentage'),
        UC_AUSPOST_PAC_MARKUP_FIXED => t('Fixed'),
      ),
      '#default_value' => $settings['markup_type'],
    ),

    'markup_amount' => array(
      '#type' => 'textfield',
      '#title' => t('Markup amount'),
      '#size' => 5,
      '#description' => t('A positive or negative number. 5 for 5% markup, or 10 for a $10 fixed markup. -10 for a $10 markdown.'),
      '#default_value' => $settings['markup_amount'],
    ),
  );

  // Allowed services.
  $services = _uc_auspost_pac_method_accessorials_options($method_id);
  $form['services'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enabled Services'),
    '#description' => t('Not all of these services will be available in all situations. Not selecting a service here enables all services.'),

    'enabled_services' => array(
      '#type' => 'checkboxes',
      '#title' => t('Enabled Services'),
      '#options' => $services,
      '#default_value' => $settings['enabled_services'],
    ),
  );

  $form['actions'] = array(
    '#type' => 'actions',

    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save settings'),
    ),
  );

  return $form;
}

/**
 * Validation handler for method configuration form.
 *
 * @see uc_auspost_pac_config_method_form().
 */
function uc_auspost_pac_config_method_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  if (!(empty($values['markup_amount']) && $values['markup_type'] == UC_AUSPOST_PAC_MARKUP_NONE)) {
    if (!is_numeric($values['markup_amount'])) {
      form_set_error('markup_amount', t('Markup amount must be a numeric value.'));
    }
  }
}

/**
 * Submit handler for method configuration form.
 *
 * @see uc_auspost_pac_config_method_form()
 */
function uc_auspost_pac_config_method_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  $method_id = $values['method_id'];
  $settings = $values['settings'];

  $settings['packing_method'] = $values['packing_method'];
  $settings['enabled_services'] = array_filter($values['enabled_services']);
  $settings['markup_type'] = $values['markup_type'];
  $settings['markup_amount'] = $values['markup_amount'];

  variable_set('uc_auspost_pac_var_method_' . $method_id, $settings);

  drupal_set_message(t('Settings have been saved.'));
}
