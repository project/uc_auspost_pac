Ubercart Australia Post Postage Assessment Calculator.

Based on the PAC PCS Dev Centre API version 1.3
http://auspost.com.au/devcentre/

State of the Module
===================

Module has now been released as 1.0 for normal use. The beta code was
effective and functional for over two years without too much of an issue.

Please review any changes listed in CHANGELOG.txt for updates and any
adjustments that may be needed prior to upgrading or installing this version.

Dev snapshots will continue between versions, so if you experience any issues
updating between them, please uninstall and re-install the module before
reporting them as issues.

There are some additional limitations on the full PAC functionality which may
make it unsuitable for your site:

- Shipping Options
Only mutually exclusive options are available for generating quotes.
Sub-options such as Extra Insurance and Delivery Confirmation are not
included. There are plans to include these with both admin configured static
options, and also end user input.

Obtaining an API key
====================

You will need a "Postage Assessment Calculator and Postcode Search API key"
for this module to function, available from Australia Post.

AusPost vary this process from time to time, without notice, so please use
your own intuition if a link no longer takes you to the right location, or
dumps you on a generic front page.

At last report, API keys were available via the Developer Center which could
be found here:
https://developers.auspost.com.au/

Your new API key will will be sent to the email address you provide. If you
have issues getting a key, please contact AusPost.

Configuring the module
======================

1. Install the module to either your global (sites/all/modules), or site
   specific (sites/<sitedir>/modules) modules directory.

2. Obtain an API Key as directed above.

3. Navigate to the module configuration page and enter the API Key. 
   Admin -> Store -> Configuration -> Shipping quotes -> Aus Post

4. Return to the Shipping quotes methods and enable the methods required.

5. Edit each method to choose which accessorials will be available to the
   client.

6. Choose and configure a packing method for each shipping method on their
   settings pages.

7. The Postcode of the site's Default pickup address MUST be set.
   - <yoursite>/admin/store/settings/quotes/settings

8. For every product to be quoted via this module it MUST have set:
   - Postal code of the product's Default product pickup address
   - Valid dimentions
   - Weight

Packing Methods
===============

To be able to quote via the AusPost PAC API, all of the products in the cart
must be grouped into parcels with a packing algorithm. Please see the README
in the packing directory for more details.

There are two packing algorithms provided in this module; Do Not Pack, and
Volume and Weight. The "Packaging" module has been brought to my attention and
it may be investigated to provide an alternative to implementing additional
strategies, or separating out the Packing API into a stand-alone module.

You may supply your own Packing algorithm by hooking into the Packing API
which is detailed uc_auspost_pac.packing.api.php.

Glacial Cache - Long term cached data
=====================================

There is a lot of information which is provided by the API that will rarely,
if ever, change. This information is kept permanently in the database table
"uc_auspost_pac_glacial", but can also optionally pull values from the caching
system bin of "cache_uc_auspost_pac_glacial". By default, the module uses a
custom get/set function pair which directly uses only the permanent data
table.

If you are only using the default Drupal caching system, you should not enable
the "Use system cache_set/cache_get functions for Glacial Cache."

This double table setup prevents the Glacial Cache values from being evicted
from memcache, breaking the module's ability to function until it is reverted
to default or manually generated.

The Glacial Cache can be manually rebuilt by clicking on Flush Glacial Cache
on the Aus Post PAC settings page.

An updated set of Glacial Cache values has been provided after the Australia
Post PAC API services changes on the 18th of April, 2016.

If you previously included the mitigation from #2416249, it should be removed
and the "Use system cache_set/cache_get functions for Glacial Cache." option
enabled in the settings.
