Packing Methods
===============

The items in the cart must be packed into packages that the AusPost PAC can
handle. There are restrictions on what is acceptable in the default settings
which can be overridden if desired.

Please see the method module README for more details of their functionality.

Method - Do Not Pack
====================

By default, the Do Not Pack method is used. It does not pack, but just
separates the cart items into the different source shipping addresses so that
each cart item can be quoted individually and added together for the quote.

This method can be useful if all of your products are shipped separately.

ALL OF YOUR PRODUCTS must meet the size restrictions for length, width,
height, girth and weight to avoid any API errors.

