<?php
/**
 * @file
 * Static data used in the Glacial Cache.
 */

/**
 * Default glacial cache. Now following the Drupal coding standards thanks to
 * drupal_var_export(). Do not add t() calls around the static strings as
 * that should be handled dynamically when pulled from the database rather
 * than stored translated.
 */
function _uc_auspost_pac_install_prebuilt_cache() {

  $country = array(
    'AF' => 'AFGHANISTAN',
    'AL' => 'ALBANIA',
    'DZ' => 'ALGERIA',
    'AS' => 'AMERICAN SAMOA',
    'AO' => 'ANGOLA',
    'AI' => 'ANGUILLA',
    'AG' => 'ANTIGUA AND BARBUDA',
    'AR' => 'ARGENTINA',
    'AM' => 'ARMENIA',
    'AW' => 'ARUBA',
    'AT' => 'AUSTRIA',
    'AZ' => 'AZERBAIJAN',
    'BS' => 'BAHAMAS',
    'BH' => 'BAHRAIN',
    'BD' => 'BANGLADESH',
    'BB' => 'BARBADOS',
    'BY' => 'BELARUS',
    'BE' => 'BELGIUM',
    'BZ' => 'BELIZE',
    'BJ' => 'BENIN',
    'BM' => 'BERMUDA',
    'BT' => 'BHUTAN',
    'BO' => 'BOLIVIA',
    'BQ' => 'BONAIRE, SINT EUSTATIUS AND SABA',
    'BA' => 'BOSNIA AND HERZEGOVINA',
    'BW' => 'BOTSWANA',
    'BR' => 'BRAZIL',
    'IO' => 'BRITISH INDIAN OCEAN TERRITORY',
    'BN' => 'BRUNEI DARUSSALAM',
    'BG' => 'BULGARIA',
    'BF' => 'BURKINA FASO',
    'BI' => 'BURUNDI',
    'KH' => 'CAMBODIA',
    'CM' => 'CAMEROON',
    'CA' => 'CANADA',
    'CV' => 'CAPE VERDE',
    'KY' => 'CAYMAN ISLANDS',
    'CF' => 'CENTRAL AFRICAN REPUBLIC',
    'TD' => 'CHAD',
    'CL' => 'CHILE',
    'CN' => 'CHINA',
    'CO' => 'COLOMBIA',
    'KM' => 'COMOROS',
    'CD' => 'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
    'CG' => 'CONGO',
    'CK' => 'COOK ISLANDS',
    'CR' => 'COSTA RICA',
    'CI' => "COTE D'IVOIRE",
    'HR' => 'CROATIA',
    'CU' => 'CUBA',
    'CW' => 'CURAÇAO',
    'CY' => 'CYPRUS',
    'CZ' => 'CZECH REPUBLIC',
    'DK' => 'DENMARK',
    'DJ' => 'DJIBOUTI',
    'DM' => 'DOMINICA',
    'DO' => 'DOMINICAN REPUBLIC',
    'EC' => 'ECUADOR',
    'EG' => 'EGYPT',
    'SV' => 'EL SALVADOR',
    'GQ' => 'EQUATORIAL GUINEA',
    'ER' => 'ERITREA',
    'EE' => 'ESTONIA',
    'ET' => 'ETHIOPIA',
    'FK' => 'FALKLAND ISLANDS (MALVINAS)',
    'FO' => 'FAROE ISLANDS',
    'FJ' => 'FIJI',
    'FI' => 'FINLAND',
    'FR' => 'FRANCE',
    'GF' => 'FRENCH GUIANA',
    'PF' => 'FRENCH POLYNESIA',
    'GA' => 'GABON',
    'GM' => 'GAMBIA',
    'GE' => 'GEORGIA',
    'DE' => 'GERMANY',
    'GH' => 'GHANA',
    'GI' => 'GIBRALTAR',
    'GR' => 'GREECE',
    'GL' => 'GREENLAND',
    'GD' => 'GRENADA',
    'GP' => 'GUADELOUPE',
    'GU' => 'GUAM',
    'GT' => 'GUATEMALA',
    'GN' => 'GUINEA',
    'GW' => 'GUINEA-BISSAU',
    'GY' => 'GUYANA',
    'HT' => 'HAITI',
    'HN' => 'HONDURAS',
    'HK' => 'HONG KONG',
    'HU' => 'HUNGARY',
    'IS' => 'ICELAND',
    'IN' => 'INDIA',
    'ID' => 'INDONESIA',
    'IR' => 'IRAN, ISLAMIC REPUBLIC OF',
    'IQ' => 'IRAQ',
    'IE' => 'IRELAND',
    'IL' => 'ISRAEL',
    'IT' => 'ITALY',
    'JM' => 'JAMAICA',
    'JP' => 'JAPAN',
    'JO' => 'JORDAN',
    'KZ' => 'KAZAKHSTAN',
    'KE' => 'KENYA',
    'KI' => 'KIRIBATI',
    'KV' => 'KOSOVO, REPUBLIC OF',
    'KW' => 'KUWAIT',
    'KG' => 'KYRGYZSTAN',
    'LA' => "LAO, PEOPLE'S DEMOCRATIC REPUBLIC",
    'LV' => 'LATVIA',
    'LB' => 'LEBANON',
    'LS' => 'LESOTHO',
    'LY' => 'LIBYA',
    'LR' => 'LIBERIA',
    'LI' => 'LIECHTENSTEIN',
    'LT' => 'LITHUANIA',
    'LU' => 'LUXEMBOURG',
    'MO' => 'MACAO',
    'MK' => 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
    'MG' => 'MADAGASCAR',
    'MW' => 'MALAWI',
    'MY' => 'MALAYSIA',
    'MV' => 'MALDIVES',
    'ML' => 'MALI',
    'MT' => 'MALTA',
    'MH' => 'MARSHALL ISLANDS',
    'MQ' => 'MARTINIQUE',
    'MR' => 'MAURITANIA',
    'MU' => 'MAURITIUS',
    'MX' => 'MEXICO',
    'FM' => 'MICRONESIA, FEDERATED STATES OF',
    'MD' => 'MOLDOVA, REPUBLIC OF',
    'MN' => 'MONGOLIA',
    'ME' => 'MONTENEGRO',
    'MS' => 'MONTSERRAT',
    'MA' => 'MOROCCO',
    'MZ' => 'MOZAMBIQUE',
    'MM' => 'BURMA (MYANMAR)',
    'NA' => 'NAMIBIA',
    'NR' => 'NAURU',
    'NP' => 'NEPAL',
    'NL' => 'NETHERLANDS',
    'NC' => 'NEW CALEDONIA',
    'NZ' => 'NEW ZEALAND',
    'NI' => 'NICARAGUA',
    'NE' => 'NIGER',
    'NG' => 'NIGERIA',
    'NU' => 'NIUE',
    'KP' => "KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF",
    'MP' => 'NORTHERN MARIANA ISLANDS',
    'NO' => 'NORWAY',
    'OM' => 'OMAN',
    'PK' => 'PAKISTAN',
    'PW' => 'PALAU',
    'PA' => 'PANAMA',
    'PG' => 'PAPUA NEW GUINEA',
    'PY' => 'PARAGUAY',
    'PE' => 'PERU',
    'PH' => 'PHILIPPINES',
    'PN' => 'PITCAIRN',
    'PL' => 'POLAND',
    'PT' => 'PORTUGAL',
    'PR' => 'PUERTO RICO',
    'QA' => 'QATAR',
    'RE' => 'REUNION',
    'RO' => 'ROMANIA',
    'RU' => 'RUSSIAN FEDERATION',
    'RW' => 'RWANDA',
    'SH' => 'ASCENSION ISLAND',
    'KN' => 'SAINT KITTS AND NEVIS',
    'MF' => 'SAINT MARTIN (FRENCH PART)',
    'LC' => 'SAINT LUCIA',
    'PM' => 'SAINT PIERRE AND MIQUELON',
    'VC' => 'SAINT VINCENT AND THE GRENADINES',
    'WS' => 'SAMOA',
    'ST' => 'SAO TOME AND PRINCIPE',
    'SA' => 'SAUDI ARABIA',
    'SN' => 'SENEGAL',
    'RS' => 'SERBIA',
    'SC' => 'SEYCHELLES',
    'SL' => 'SIERRA LEONE',
    'SG' => 'SINGAPORE',
    'SK' => 'SLOVAKIA',
    'SI' => 'SLOVENIA',
    'SB' => 'SOLOMON ISLANDS',
    'SO' => 'SOMALIA',
    'ZA' => 'SOUTH AFRICA',
    'KR' => 'KOREA, REPUBLIC OF',
    'ES' => 'SPAIN',
    'LK' => 'SRI LANKA',
    'SD' => 'SUDAN',
    'SR' => 'SURINAME',
    'SZ' => 'SWAZILAND',
    'SE' => 'SWEDEN',
    'CH' => 'SWITZERLAND',
    'SY' => 'SYRIAN ARAB REPUBLIC',
    'TW' => 'TAIWAN',
    'TJ' => 'TAJIKISTAN',
    'TZ' => 'TANZANIA, UNITED REPUBLIC OF',
    'TH' => 'THAILAND',
    'TL' => 'TIMOR LESTE',
    'TG' => 'TOGO',
    'TK' => 'TOKELAU',
    'TO' => 'TONGA',
    'TT' => 'TRINIDAD AND TOBAGO',
    'TN' => 'TUNISIA',
    'TR' => 'TURKEY',
    'TM' => 'TURKMENISTAN',
    'TC' => 'TURKS AND CAICOS ISLANDS',
    'TV' => 'TUVALU',
    'UG' => 'UGANDA',
    'UA' => 'UKRAINE',
    'AE' => 'UNITED ARAB EMIRATES',
    'GB' => 'UNITED KINGDOM',
    'US' => 'UNITED STATES',
    'UM' => 'UNITED STATES MINOR OUTLYING ISLANDS',
    'UY' => 'URUGUAY',
    'UZ' => 'UZBEKISTAN',
    'VU' => 'VANUATU',
    'VG' => 'VIRGIN ISLANDS, BRITISH',
    'VI' => 'VIRGIN ISLANDS, U.S.',
    'VA' => 'HOLY SEE (VATICAN CITY STATE)',
    'VE' => 'VENEZUELA',
    'VN' => 'VIETNAM',
    'WF' => 'WALLIS AND FUTUNA',
    'YE' => 'YEMEN',
    'ZM' => 'ZAMBIA',
    'ZW' => 'ZIMBABWE',
  );

  $domestic_letter_services = array(
    'AUS_LETTER_REGULAR_SMALL' => array(
      'name' => 'Small Letter',
      'max_extra_cover' => 5000,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
        ),
        'AUS_SERVICE_OPTION_REGISTERED_POST' => array(
          'name' => 'Registered Post',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_DELIVERY_CONFIRMATION' => array(
              'name' => 'Delivery Confirmation',
            ),
            'AUS_SERVICE_OPTION_PERSON_TO_PERSON' => array(
              'name' => 'Person to Person',
            ),
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_LETTER_PRIORITY_SMALL' => array(
      'name' => 'Small Priority Letter',
      'max_extra_cover' => 5000,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
        ),
        'AUS_SERVICE_OPTION_REGISTERED_POST' => array(
          'name' => 'Registered Post',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_DELIVERY_CONFIRMATION' => array(
              'name' => 'Delivery Confirmation',
            ),
            'AUS_SERVICE_OPTION_PERSON_TO_PERSON' => array(
              'name' => 'Person to Person',
            ),
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_LETTER_EXPRESS_SMALL' => array(
      'name' => 'Express Post Small Envelope',
      'max_extra_cover' => 5000,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
        'AUS_SERVICE_OPTION_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature On Delivery',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_LETTER_EXPRESS_MEDIUM' => array(
      'name' => 'Express Post Medium Envelope',
      'max_extra_cover' => 5000,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
        'AUS_SERVICE_OPTION_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature On Delivery',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_LETTER_REGULAR_LARGE_125' => array(
      'name' => 'Large Letter',
      'max_extra_cover' => 5000,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
        ),
        'AUS_SERVICE_OPTION_REGISTERED_POST' => array(
          'name' => 'Registered Post',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_DELIVERY_CONFIRMATION' => array(
              'name' => 'Delivery Confirmation',
            ),
            'AUS_SERVICE_OPTION_PERSON_TO_PERSON' => array(
              'name' => 'Person to Person',
            ),
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_LETTER_PRIORITY_LARGE_125' => array(
      'name' => 'Large Priority Letter',
      'max_extra_cover' => 5000,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
        ),
        'AUS_SERVICE_OPTION_REGISTERED_POST' => array(
          'name' => 'Registered Post',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_DELIVERY_CONFIRMATION' => array(
              'name' => 'Delivery Confirmation',
            ),
            'AUS_SERVICE_OPTION_PERSON_TO_PERSON' => array(
              'name' => 'Person to Person',
            ),
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_LETTER_EXPRESS_LARGE' => array(
      'name' => 'Express Post Large Envelope',
      'max_extra_cover' => 5000,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
        'AUS_SERVICE_OPTION_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature On Delivery',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_LETTER_REGULAR_LARGE_250' => array(
      'name' => 'Large Letter',
      'max_extra_cover' => 5000,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
        ),
        'AUS_SERVICE_OPTION_REGISTERED_POST' => array(
          'name' => 'Registered Post',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_DELIVERY_CONFIRMATION' => array(
              'name' => 'Delivery Confirmation',
            ),
            'AUS_SERVICE_OPTION_PERSON_TO_PERSON' => array(
              'name' => 'Person to Person',
            ),
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_LETTER_PRIORITY_LARGE_250' => array(
      'name' => 'Large Priority Letter',
      'max_extra_cover' => 5000,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
        ),
        'AUS_SERVICE_OPTION_REGISTERED_POST' => array(
          'name' => 'Registered Post',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_DELIVERY_CONFIRMATION' => array(
              'name' => 'Delivery Confirmation',
            ),
            'AUS_SERVICE_OPTION_PERSON_TO_PERSON' => array(
              'name' => 'Person to Person',
            ),
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_LETTER_REGULAR_LARGE_500' => array(
      'name' => 'Large Letter',
      'max_extra_cover' => 5000,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
        ),
        'AUS_SERVICE_OPTION_REGISTERED_POST' => array(
          'name' => 'Registered Post',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_DELIVERY_CONFIRMATION' => array(
              'name' => 'Delivery Confirmation',
            ),
            'AUS_SERVICE_OPTION_PERSON_TO_PERSON' => array(
              'name' => 'Person to Person',
            ),
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_LETTER_PRIORITY_LARGE_500' => array(
      'name' => 'Large Priority Letter',
      'max_extra_cover' => 5000,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
        ),
        'AUS_SERVICE_OPTION_REGISTERED_POST' => array(
          'name' => 'Registered Post',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_DELIVERY_CONFIRMATION' => array(
              'name' => 'Delivery Confirmation',
            ),
            'AUS_SERVICE_OPTION_PERSON_TO_PERSON' => array(
              'name' => 'Person to Person',
            ),
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
  );

  $domestic_parcel_services = array(
    'AUS_PARCEL_EXPRESS' => array(
      'name' => 'Express Post',
      'max_extra_cover' => 300,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
        'AUS_SERVICE_OPTION_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on Delivery',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_PARCEL_EXPRESS_SATCHEL_3KG' => array(
      'name' => 'Express Post Medium (3Kg) Satchel',
      'max_extra_cover' => 300,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
        'AUS_SERVICE_OPTION_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on Delivery',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_PARCEL_REGULAR' => array(
      'name' => 'Parcel Post',
      'max_extra_cover' => 300,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
        'AUS_SERVICE_OPTION_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on Delivery',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_PARCEL_REGULAR_SATCHEL_3KG' => array(
      'name' => 'Parcel Post Medium Satchel',
      'max_extra_cover' => 300,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
        'AUS_SERVICE_OPTION_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on Delivery',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_PARCEL_EXPRESS_SATCHEL_5KG' => array(
      'name' => 'Express Post Large (5Kg) Satchel',
      'max_extra_cover' => 300,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
        'AUS_SERVICE_OPTION_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on Delivery',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_PARCEL_REGULAR_SATCHEL_5KG' => array(
      'name' => 'Parcel Post Large Satchel',
      'max_extra_cover' => 300,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
        'AUS_SERVICE_OPTION_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on Delivery',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_PARCEL_EXPRESS_SATCHEL_500G' => array(
      'name' => 'Express Post Small Satchel',
      'max_extra_cover' => 300,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
        'AUS_SERVICE_OPTION_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on Delivery',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
    'AUS_PARCEL_REGULAR_SATCHEL_500G' => array(
      'name' => 'Parcel Post Small Satchel',
      'max_extra_cover' => 300,
      'options' => array(
        'AUS_SERVICE_OPTION_STANDARD' => array(
          'name' => 'Standard Service',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
        'AUS_SERVICE_OPTION_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on Delivery',
          'suboptions' => array(
            'AUS_SERVICE_OPTION_EXTRA_COVER' => array(
              'name' => 'Extra Cover',
            ),
          ),
        ),
      ),
    ),
  );

  $international_letter_services = array(
    'INT_LETTER_COR_OWN_PACKAGING' => array(
      'name' => 'Courier',
      'max_extra_cover' => 5000,
      'options' => array(
        'INT_TRACKING' => array(
          'name' => 'Tracking',
        ),
        'INT_SMS_TRACK_ADVICE' => array(
          'name' => 'SMS track advice',
        ),
        'INT_EXTRA_COVER' => array(
          'name' => 'Extra Cover',
        ),
      ),
    ),
    'INT_LETTER_EXP_OWN_PACKAGING' => array(
      'name' => 'Express',
      'max_extra_cover' => 5000,
      'options' => array(
        'INT_TRACKING' => array(
          'name' => 'Tracking',
        ),
        'INT_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on delivery',
        ),
        'INT_SMS_TRACK_ADVICE' => array(
          'name' => 'SMS track advice',
        ),
        'INT_EXTRA_COVER' => array(
          'name' => 'Extra Cover',
        ),
      ),
    ),
    'INT_LETTER_REG_SMALL_ENVELOPE' => array(
      'name' => 'Registered Post DL',
      'options' => array(
        'INT_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on delivery',
        ),
      ),
    ),
    'INT_LETTER_REG_LARGE_ENVELOPE' => array(
      'name' => 'Registered Post B4',
      'options' => array(
        'INT_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on delivery',
        ),
      ),
    ),
    'INT_LETTER_AIR_OWN_PACKAGING_LIGHT' => array(
      'name' => 'Economy Air',
      'max_extra_cover' => 500,
      'options' => array(
        '' => array(
          'name' => NULL,
        ),
      ),
    ),
    'INT_LETTER_AIR_OWN_PACKAGING_MEDIUM' => array(
      'name' => 'Economy Air',
      'max_extra_cover' => 500,
      'options' => array(
        '' => array(
          'name' => NULL,
        ),
      ),
    ),
    'INT_LETTER_AIR_OWN_PACKAGING_HEAVY' => array(
      'name' => 'Economy Air',
      'max_extra_cover' => 500,
      'options' => array(
        '' => array(
          'name' => NULL,
        ),
      ),
    ),
  );

  $international_parcel_services = array(
    'INT_PARCEL_COR_OWN_PACKAGING' => array(
      'name' => 'Courier',
      'max_extra_cover' => 5000,
      'options' => array(
        'INT_TRACKING' => array(
          'name' => 'Tracking',
        ),
        'INT_SMS_TRACK_ADVICE' => array(
          'name' => 'SMS track advice',
        ),
        'INT_EXTRA_COVER' => array(
          'name' => 'Extra Cover',
        ),
      ),
    ),
    'INT_PARCEL_EXP_OWN_PACKAGING' => array(
      'name' => 'Express',
      'max_extra_cover' => 5000,
      'options' => array(
        'INT_TRACKING' => array(
          'name' => 'Tracking',
        ),
        'INT_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on delivery',
        ),
        'INT_SMS_TRACK_ADVICE' => array(
          'name' => 'SMS track advice',
        ),
        'INT_EXTRA_COVER' => array(
          'name' => 'Extra Cover',
        ),
      ),
    ),
    'INT_PARCEL_STD_OWN_PACKAGING' => array(
      'name' => 'Standard',
      'max_extra_cover' => 5000,
      'options' => array(
        'INT_TRACKING' => array(
          'name' => 'Tracking',
        ),
        'INT_EXTRA_COVER' => array(
          'name' => 'Extra Cover',
        ),
        'INT_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on delivery',
        ),
        'INT_SMS_TRACK_ADVICE' => array(
          'name' => 'SMS track advice',
        ),
      ),
    ),
    'INT_PARCEL_AIR_OWN_PACKAGING' => array(
      'name' => 'Economy Air',
      'max_extra_cover' => 5000,
      'options' => array(
        'INT_EXTRA_COVER' => array(
          'name' => 'Extra Cover',
        ),
        'INT_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on delivery',
        ),
      ),
    ),
    'INT_PARCEL_SEA_OWN_PACKAGING' => array(
      'name' => 'Economy Sea',
      'max_extra_cover' => 5000,
      'options' => array(
        'INT_EXTRA_COVER' => array(
          'name' => 'Extra Cover',
        ),
        'INT_SIGNATURE_ON_DELIVERY' => array(
          'name' => 'Signature on delivery',
        ),
      ),
    ),
  );

  // Add each item to the cache now.
  uc_auspost_pac_glacial_set('appac:country', $country);
  uc_auspost_pac_glacial_set('appac:auspost_domestic_letter', $domestic_letter_services);
  uc_auspost_pac_glacial_set('appac:auspost_domestic_parcel', $domestic_parcel_services);
  uc_auspost_pac_glacial_set('appac:auspost_intnl_letter', $international_letter_services);
  uc_auspost_pac_glacial_set('appac:auspost_intnl_parcel', $international_parcel_services);
  uc_auspost_pac_glacial_set('appac:built-marker', array('source' => 'install', 'created' => 1472886219));
}
