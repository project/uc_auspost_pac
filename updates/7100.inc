<?php
/**
 * @file
 * Update 7100 - Glacial cache updates.
 */

/**
 * Update the items in the Glacial Cache to post April 8th 2013 values.
 */
function _uc_auspost_pac_update_7100(&$sandbox) {

  $t = get_t();

  // Gather the current state of the glacial cache.
  $glacial_info = _uc_auspost_pac_glacial_cache_info();

  if ($glacial_info['source'] == 'install' || $glacial_info['source'] == 'unknown') {
    // Update to the latest values.
    module_load_include('inc', 'uc_auspost_pac', 'uc_auspost_pac.static_data');
    _uc_auspost_pac_install_prebuilt_cache();

    return $t('The Glacial Cache has been updated with new services. Please see <a href="@url">#1968420 - AusPost API changes from 8 April 2013</a> for more information.', array(
      '@url' => 'http://drupal.org/node/1968420',
    ));
  }

  else {
    return $t('Glacial Cache has not been updated automatically because you have manually regenerated it, and will have to do so again now. Please see <a href="@url">#1968420 - AusPost API changes from 8 April 2013</a> for more information.', array(
      '@url' => 'http://drupal.org/node/1968420',
    ));
  }
}
