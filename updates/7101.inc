<?php
/**
 * @file
 * Update 7101 - Glacial cache updates.
 */

/**
 * Update the items in the Glacial Cache to post April 18th 2016 values.
 */
function _uc_auspost_pac_update_7101(&$sandbox) {

  $t = get_t();
  $messages = array();

  // New database table now exists to permanently store the Glacial Cache when
  // this module is used in combination with memcache or similar.
  if (!db_table_exists('uc_auspost_pac_glacial')) {
    // We can use the schema from hook_schema instead of a static table
    // because we're just copying the system cache table. db_create_table does
    // not return a value.
    db_create_table(
      'uc_auspost_pac_glacial',
      drupal_get_schema_unprocessed('uc_auspost_pac', 'uc_auspost_pac_glacial')
    );

    $messages[] = $t('Added new Glacial Cache table.');
  }

  // Use the new table directly, without cache_get.
  variable_set('uc_auspost_pac_var_use_cache_getset', FALSE);

  // Kill off no longer used variable.
  variable_del('uc_auspost_pac_flush_glacial_cache');

  // At this point, we have the unique situation where all of our functions
  // are rigged for two tables, but in realtity we only have one with data
  // in it. We get around this by pulling all of the current data and adding
  // it to the new table.
  $marker = cache_get('appac:built-marker', 'cache_uc_auspost_pac_glacial');
  if (!$marker) {
    // Install the default table. The site is presently broken.
    module_load_include('inc', 'uc_auspost_pac', 'uc_auspost_pac.static_data');
    _uc_auspost_pac_install_prebuilt_cache();
  }
  else {
    // Pull values from the cache, and set them using the new functions.
    $known_cids = array(
      'appac:country',
      'appac:auspost_domestic_letter',
      'appac:auspost_domestic_parcel',
      'appac:auspost_intnl_letter',
      'appac:auspost_intnl_parcel',
      'appac:built-marker',
    );
    foreach ($known_cids as $cid) {
      $item = cache_get($cid, 'cache_uc_auspost_pac_glacial');
      if ($item) {
        uc_auspost_pac_glacial_set($cid, $item->data);
      }
    }
  }

  // We do need to flush the glacial cache bin just in case it still holds any
  // out of date values.
  cache_clear_all(NULL, 'cache_uc_auspost_pac_glacial');

  // Now that we have a semi-known state, update the glacial values as per
  // normal. Gather the current state of the glacial cache, and update
  // everything as needed.
  $glacial_info = _uc_auspost_pac_glacial_cache_info();

  if ($glacial_info['source'] == 'install' || $glacial_info['source'] == 'unknown') {
    // Update to the latest values.
    module_load_include('inc', 'uc_auspost_pac', 'uc_auspost_pac.static_data');
    _uc_auspost_pac_install_prebuilt_cache();

    $messages[] = $t('The Glacial Cache has been updated with new services. Please see <a href="@url">#2695803 - AusPost API changes from 18 April 2016</a> for more information.', array(
      '@url' => 'https://www.drupal.org/node/2695803',
    ));
  }

  else {
    $messages[] = $t('Glacial Cache has not been updated automatically because you have manually regenerated it, and will have to do so again now. Please see <a href="@url">#2695803 - AusPost API changes from 18 April 2016</a> for more information.', array(
      '@url' => 'https://www.drupal.org/node/2695803',
    ));
  }

  if (count($messages) > 1) {
    // Return messages as a unordered list.
    return '<ul><li>' . implode('</li><li>', $messages) . '</li></ul>';
  }
  else {
    // Always at least 1 message.
    return reset($messages);
  }
}
