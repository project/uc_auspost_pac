# Updating - 7.x-1.0beta1 to 7.x-1.1

How the Glacial Cache (GC) is handled has changed in this version, and it no longer requires special configuration to handle memcache or other such systems.

If your site has manually set cache_class_cache_uc_auspost_pac_glacial to DrupalDatabaseCache, it should be removed as this is now a fully functioning cache bin which no longer needs special handling.

The GC now uses a separate permanent values database table, and an optional cache bin to allow for memcache to tie in if required. To avoid storing the data twice by default, the module uses custom get/set function which directly access the permanent table, bypassing the caching system. It is functionally identical to the system database cache.

If you wish to use the caching system for the Glacial Cache, you will need to enable "Use system cache_set/cache_get functions for Glacial Cache."

For more details on this issue, please see https://www.drupal.org/node/2416249
